// Compile-time config flags

// Enable if btime is installed
//#define PRECISE_TIMER_AVAILABLE

// Debug flags

// Delete queue debug toggle
// This is expensive. don't turn it on on the server unless you want things to be bad and slow
// #define DELETE_QUEUE_//DEBUG

//#define UPDATE_QUEUE_//DEBUG

// Image deletion debug
// DO NOT ENABLE THIS ON THE SERVER FOR FUCKS SAKE
//#define IMAGE_DEL_//DEBUG

// Machine processing debug
//Apparently not that hefty but still
//#define MACHINE_PROCESSING_//DEBUG

//Queue worker statistics
//Probably hefty
//#define QUEUE_STAT_//DEBUG

// Map overrides
// Construction mode
//#define MAP_OVERRIDE_CONSTRUCTION
// Destiny/RP
//#define MAP_OVERRIDE_DESTINY

//#define MAP_OVERRIDE_TEST